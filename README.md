# My patched version of surf

This is a patched verion of surf made for my taste. The config.h file contains the settings I use but the config.def.h if you want to modify settings for your own use.

## Patches

My version contains the following patches

1. [Search engines patch](https://surf.suckless.org/patches/searchengines/surf-0.7-webkit2-searchengines.diff)
2. [Home page patch](https://surf.suckless.org/patches/homepage/surf-2.0-homepage.diff)
3. [Quit hotkey patch](https://surf.suckless.org/patches/quit-hotkey/surf-quit_hotkey-20210830-11dca18.diff)


## Requirements

In order to build surf you need GTK+ and Webkit/GTK+ header files.

In order to use the functionality of the url-bar, also install dmenu[0].


## Installation

Edit config.mk to match your local setup (surf is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install surf (if
necessary as root):

    make clean install
    or
    sudo make clean install


## Running surf
run
	surf [URI]

See the manpage for further options.


## Running surf in tabbed

For running surf in tabbed[1] there is a script included in the distribution,
which is run like this:

	surf-open.sh [URI]

Further invocations of the script will run surf with the specified URI in this
instance of tabbed.

[0] http://tools.suckless.org/dmenu
[1] http://tools.suckless.org/tabbed

